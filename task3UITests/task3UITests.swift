//
//  task3UITests.swift
//  task3UITests
//
//  Created by iosdev on 25.3.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import XCTest

class task3UITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    /*func testButton() {
        let app = XCUIApplication()
        app.launch()
        let button = app.buttons["Save"]
        XCTAssertFalse(button.exists);
        // let text = XCUIApplication().textFields.element(boundBy: 0)
        let nameTextField = app.textFields.element
        nameTextField.tap()
        nameTextField.typeText("Hello John")

        //app.textFields.element.tap()
        XCTAssertTrue(button.exists);
        
    }*/
    
    func testExample() {
        // UI tests must launch the application that they test.
        //let app = XCUIApplication()
        
        let app = XCUIApplication()
        app.launch()
        
        /*app.navigationBars["History"].buttons["Add"].tap()
        
        let pickersQuery = app.pickers
        let pickerWheel = pickersQuery.children(matching: .pickerWheel).element(boundBy: 1)
        pickerWheel.tap()
        pickerWheel/*@START_MENU_TOKEN@*/.press(forDuration: 1.4);/*[[".tap()",".press(forDuration: 1.4);"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        pickersQuery.children(matching: .pickerWheel).element(boundBy: 0).tap()
        
        let enterNameTextField = app.textFields["Enter name"]
        enterNameTextField.tap()
        
        let element = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element
        element.children(matching: .other).element(boundBy: 1).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.tap()
        enterNameTextField.tap()
        
        let table = element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .table).element
        
        XCTAssertEqual(table.cells.count, table.cells.count + 1, "There should be 1 rows more")
        //XCTAssertTrue(app.textFields["Enter name"].exists)*/

    }
    

    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}

