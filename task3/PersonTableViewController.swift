//
//  PersonTableViewController.swift
//  task3
//
//  Created by iosdev on 26.3.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class PersonTableViewController: UITableViewController , UINavigationControllerDelegate{
    //MARK: Properties
     
    var persons = [Person]()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = editButtonItem

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return persons.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "PersonTableViewCell"

        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PersonTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }

        let person = persons[indexPath.row]
        
        if person.bmi<=18 || person.bmi > 30 {
            cell.contentView.backgroundColor = UIColor.red
        } else if person.bmi >= 25 && person.bmi <= 30 {
            cell.contentView.backgroundColor = UIColor.yellow
        } else {
            cell.contentView.backgroundColor = UIColor.green
        }
        cell.nameLabel.text = person.name
        cell.heightLabel.text = String(person.height)
        cell.weightLabel.text = String(person.weight)
        cell.bmiLabel.text = String(person.bmi)

    
        
        return cell
    }
    /*func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let backgroundView = UIView()
        let person = persons[indexPath.row]

        if person.bmi<=18 || person.bmi > 30 {
            backgroundView.backgroundColor = UIColor.red
        } else if person.bmi >= 25 && person.bmi <= 30 {
            backgroundView.backgroundColor = UIColor.yellow
        } else {
            backgroundView.backgroundColor = UIColor.green
        }
        cell.selectedBackgroundView = backgroundView
        
    }*/

    @IBAction func unwindToMealList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? ViewController, let person = sourceViewController.person {
            
            // Add a new meal.
            let newIndexPath = IndexPath(row: persons.count, section: 0)
            
            persons.append(person)
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        }
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            persons.remove(at: indexPath.row)

            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
