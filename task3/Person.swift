//
//  Person.swift
//  task3
//
//  Created by iosdev on 25.3.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit

class Person {
    //MARK: Properties
    
    var name: String
    var bmi: Double
    var height: Int
    var weight: Int
    
    
    //MARK: Initialization
     
    init(name: String, bmi: Double, height: Int, weight: Int) {
        self.name = name
        self.bmi = bmi
        self.height = height
        self.weight = weight
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
