//
//  ViewController.swift
//  task3
//
//  Created by iosdev on 25.3.2020.
//  Copyright © 2020 iosdev. All rights reserved.
//

import UIKit
import os.log


class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate,
    UINavigationControllerDelegate
 {
    

    @IBOutlet weak var index: UILabel!
    @IBOutlet weak var nameInput: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBOutlet weak var weightPicker: UIPickerView!
    
    @IBOutlet weak var saveBttn: UIButton!
    //@IBOutlet weak var heightPicker: UIPickerView!
    
    var heightData = Array(30...210)
    var weightData = Array(3...200)
    
    var labelString = ""
    var person: Person?
    var height: Int = 0
    var weight: Int = 0
    var decBmi: Double = 0.0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.nameInput.delegate = self

            updateSaveButtonState()

        //heightPicker.delegate = self as UIPickerViewDelegate
        //heightPicker.dataSource = self as UIPickerViewDataSource
        //self.heightPicker.reloadAllComponents()

        
        weightPicker.delegate = self as UIPickerViewDelegate
        weightPicker.dataSource = self as UIPickerViewDataSource
        //weightPicker.reloadAllComponents()
        //weightPicker.delegate?.pickerView?(weightPicker, didSelectRow: 50, inComponent: 0)
        //weightPicker.delegate?.pickerView?(weightPicker, didSelectRow: 125, inComponent: 1)
        
        //let heightDataInt = Array(30...210)
        //heightData = heightDataInt.map{ String($0) }
        //let weightDataInt = Array(3...200)
        //weightData = weightDataInt.map{ String($0) }
        weightPicker.selectRow(125, inComponent: 1, animated: true)
        weightPicker.selectRow(50, inComponent: 0, animated: true)

    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0{
            return weightData.count
        } else {
            return heightData.count
        }
    }
    
    private func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return String(weightData[row])
        } else {
            return String(heightData[row])
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.letters
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        weight = weightData[weightPicker.selectedRow(inComponent: 0)]
        height = heightData[weightPicker.selectedRow(inComponent: 1)]
        print(weight, height)
        let bmi = Double(weight)/pow(Double(height)/100,2)
        decBmi = Double(round(10 * bmi)/10)
        print(decBmi)
        index.text = "\(decBmi)"
        resultLabel.text = "Height: \(height), weight: \(weight)"
        if component == 0 {
            print("weight \(weightData[row])")
        }
        else{
            print("height \(heightData[row])")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        // Configure the destination view controller only when the save button is pressed.
        guard let button = sender as? UIButton, button === saveBttn else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        
        let name = nameInput.text ?? ""
        // Set the meal to be passed to MealTableViewController after the unwind segue.
        person = Person(name: name, bmi: decBmi, height: height, weight: weight)
        print(height, weight, decBmi, name)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        // Disable the Save button while editing.
        updateSaveButtonState()
    }
    
    private func updateSaveButtonState() {
        // Disable the Save button if the text field is empty.
        let text = nameInput.text ?? ""
        print(text)
        saveBttn.isEnabled = !text.isEmpty
    }
}




